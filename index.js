const express = require("express");
const ROUTES = require("./routes.json");
const {setupProxies} = require("./proxy");

const app_express = express();
const PORT = process.env.PORT || 4001;

setupProxies(app_express, ROUTES);
app_express.listen(PORT, () => {
    console.log(`Gateway listening at http://0.0.0.0:${PORT}`)
});